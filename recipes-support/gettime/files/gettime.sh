#!/bin/sh
# This script waits for network connection by pinging specified host
# and get the time from the specified ntp server.
# It is used to set the initial time at boot time

VERSION="0.5"
VERBOSE=''
__USESYSLOG=1
SYNCDIR=/var/run/
SYNCFILE=gettime.sync

# Get configuration file
source /etc/gettime.conf

function log_write()
{
	if [ ! -z ${VERBOSE} ] || [ ${__USESYSLOG} -eq 0 ]; then
		echo "$*"
	fi
	if [ ${__USESYSLOG} -ne 0 ] ; then
		logger -p local4.info  "$*"
	fi
}

function execute()
{
	$* > /dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: executing $*"
                if [ ${__USESYSLOG} -ne 0 ]; then
		        logger -p local4.info  "ERROR: $*"
		fi
	fi
}

function version()
{
	echo
	echo "`basename $1` version ${VERSION}"
	echo "Script to get time for Litecell15 platform"
	echo
	exit 0
}

function usage()
{
	cat <<EOM
	Usage:
	$0 [-d] [-v] [-h]

	Parameters:
	-d: print debug
	-v: Script version
	-h: This message

EOM
	exit 0
}

#############################################
#
# Main routine
#
#############################################

# Parse the input options
while getopts :dvh ARG
do
	case ${ARG} in
		d ) VERBOSE=1;;
		v ) version $0;;
		h ) usage;;
		\?) echo "Invalid option: -$OPTARG" && exit 1;;
	esac
done

log_write "Litecell15 gettime script started"

if [ -f $SYNCDIR/$SYNCFILE ]; then
    __RM_RES=$(rm -f $SYNCDIR/$SYNCFILE)
    __RM_RET=$?
    if test ${__RM_RET} -ne 0; then
		log_write "Failed to erase sync file"
    fi
fi

__NTPD_ACT=$(systemctl is-active ntpd)
__NTPD_RET=$?
log_write "ntpd state: ${__NTPD_ACT}"
if test ${__NTPD_RET} -eq 0; then
    log_write "Disabling ntpd..."
    __NTPP_RES=$(systemctl stop ntpd)
    log_write $__NTPP_RES
fi
__COUNTER=1
if test ${__TESTNET} -eq 1; then
	log_write "Testing for network..."
	until test $__COUNTER -gt ${__TRYMAX}; do
	__PING_RES=$(ping -q -c3 ${__PINGSERVER})
	__PING_RET=$?
	log_write $__PING_RES
	if test ${__PING_RET} -eq 0; then
		break;
	fi
	__COUNTER=$((__COUNTER+1))
	if test $__COUNTER -le ${__TRYMAX}; then
		log_write "Waiting for network..."
	fi
	done
fi
if test $__COUNTER -le ${__TRYMAX}; then
	log_write "Trying to update system time..."
	__DATE_RES=$(ntpdate -b -t ${__NTPTOUT} ${__NTPSERVER})
	__DATE_RET=$?
	log_write $__DATE_RES
	if test ${__DATE_RET} -eq 0; then
		log_write "Creating sync file..."
		if [ ! -d $SYNCDIR ]; then
			__MK_RES=$(mkdir -p $SYNCDIR)
			__MK_RET=$?
			if test ${__MK_RET} -ne 0; then
				log_write "Failed to create sync directory!"
			fi
		fi
		echo $__DATE_RES > $SYNCDIR/$SYNCFILE
		__SYNCFILE_RET=$?
		if test ${__SYNCFILE_RET} -eq 0; then
			log_write "Done."
		else
			log_write "Failed to update sync file!"
		fi
	else
		log_write "Failed to update time from ${__NTPSERVER}!"
	fi
else
	log_write "Failed to connect to ${__PINGSERVER}, time not set!"
fi
if test ${__NTPD_RET} -eq 0; then
    log_write "Enabling ntpd..."
    __NTPT_RES=$(systemctl start ntpd)
    log_write $__NTPT_RES
fi
log_write "Litecell15 gettime done!"

