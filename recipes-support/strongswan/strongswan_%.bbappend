FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += " \
           file://strongswan.service \
           "

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

do_install_append() {
	# default configuration directory
	install -d 0755 ${D}${sysconfdir}/defaultconfig
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config

	# install default strongswan configuration
	install -m 0644 ${D}${sysconfdir}/ipsec.conf ${D}${sysconfdir}/defaultconfig/config/ipsec_template.conf
	rm -rf ${D}${sysconfdir}/ipsec.conf
	ln -sf /mnt/rom/user/config/ipsec.conf ${D}${sysconfdir}/ipsec.conf
	install -m 0600 ${D}${sysconfdir}/ipsec.secrets ${D}${sysconfdir}/defaultconfig/config/ipsec.secrets
	rm -rf ${D}${sysconfdir}/ipsec.secrets
	ln -sf /mnt/rom/user/config/ipsec.secrets ${D}${sysconfdir}/ipsec.secrets
	install -m 0644 ${D}${sysconfdir}/strongswan.conf ${D}${sysconfdir}/defaultconfig/config/strongswan.conf
	rm -rf ${D}${sysconfdir}/strongswan.conf
	ln -sf /mnt/rom/user/config/strongswan.conf ${D}${sysconfdir}/strongswan.conf

	# install strongswan service
	install -m 644 ${WORKDIR}/${PN}.service ${D}/${systemd_unitdir}/system

	# enable systemd on sysinit
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
	ln -sf ../${PN}.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
}

FILES_${PN} += "${systemd_unitdir} \
	${sysconfdir}/defaultconfig/config/* \
	"
