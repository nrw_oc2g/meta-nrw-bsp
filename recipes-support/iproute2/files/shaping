#!/bin/bash
#
# Traffic shaping sample configuration file
#
#  tc uses the following units when passed as a parameter.
#  kbps: Kilobytes per second
#  mbps: Megabytes per second
#  kbit: Kilobits per second
#  mbit: Megabits per second
#  bps: Bytes per second
#       Amounts of data can be specified in:
#       kb or k: Kilobytes
#       mb or m: Megabytes
#       mbit: Megabits
#       kbit: Kilobits
#  To get the byte figure from bits, divide the number by 8 bit

# Uncomment to enable traffic shaping
#ENABLE=true

# Ethernet device
DEV=$2

# Maximum allowed bandwidth
DEV_MAX_BW=384kbit	# Device

# R2Q paramater
# Use 1 if one rate is below 64 kbit, 10 otherwise
R2Q=10

start() {
	tc qdisc add dev $DEV root handle 1: htb default 20 r2q $R2Q

        tc class add dev $DEV parent 1: classid 1:20  htb \
                rate $DEV_MAX_BW ceil $DEV_MAX_BW

        tc qdisc add dev $DEV parent 1:20 handle 20: sfq perturb 10
}

stop() {
	tc qdisc del dev $DEV root
}

restart() {
	stop
	sleep 1
	start
}

show() {
	tc -s -d qdisc show
}



case "$1" in

	start)
		echo -n "Starting traffic shaping: "
		[ "$ENABLE" = true ] && start
		echo "done"	
		;;

	stop)
		echo -n "Stopping traffic shaping: "
		[ "$ENABLE" = true ] && stop
		echo "done"
		;;

	restart)
		echo -n "Restarting traffic shaping: "
		restart
		echo "done"
		;;

	show)
		echo "Bandwidth shaping status:"
		show
		echo ""
		;;

	*)
		pwd=$(pwd)
		echo "Usage: shaping {start|stop|restart|show}"
		;;

esac

exit 0
