inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "libtalloc_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

FILES_${PN} = "${libdir}/${PN}.so.${PV:0:2} \
                   ${libdir}/${PN}.so.2 \
                   ${libdir}/${PN}.so.${PV} \
                   ${libdir}/libpytalloc-util.so.${PV}"
FILES_${PN}-dbg = "/usr/src/debug/ \
                   ${libdir}/.debug/${PN}.so.${PV}"
FILES_${PN}-dev = "${includedir}/ \
                   ${libdir}/${PN}.so \
                   ${libdir}/pkgconfig/"


