SUMMARY = "The ADAPTIVE Communication Environment (ACE)"
HOMEPAGE = "http://www.cs.wustl.edu/~schmidt/ACE.html"
SECTION = "libs"
LICENSE = "DOC"
LIC_FILES_CHKSUM = "file://COPYING;md5=d44c94111b5ebbca846989113d1528e8"

inherit gitver-repo
 
REPODIR   = "${THISDIR}"
REPOFILE  = "libace_6.0.3.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

inherit autotools pkgconfig perlnative

EXTRA_OECONF += "--disable-ace-examples \
--disable-ace-tests \
--disable-winregistry \
--disable-wfmo \
--disable-wince \
--enable-shared \
--disable-gperf"

PARALLEL_MAKE = ""

DEPENDS = "libtool openssl ncurses zlib libtirpc"

SRC_URI = "http://download.dre.vanderbilt.edu/previous_versions/ACE-6.0.3.tar.bz2 \
file://ACE.mwc"

SRC_URI[md5sum] = "c38cff517ee80825a37f3b1e84f15229"
SRC_URI[sha256sum] = "411e241e34c621ea0ef4a5b87c0fb73654180a96ca7ed3c1dc8d2c3b841c7852"

S = "${WORKDIR}/ACE_wrappers"

CFLAGS += "-I../../ACE_wrappers -I../../../ACE_wrappers -I../../../../ACE_wrappers -I../../../../../ACE_wrappers"
CXXFLAGS += "-I../../ACE_wrappers -I../../../ACE_wrappers -I../../../../ACE_wrappers -I../../../../../ACE_wrappers"

PLATFORM_MACROS = "${S}/include/makeinclude/platform_macros.GNU"

do_configure_prepend() {
    export ACE_ROOT=${S}
    
    echo "#include \"ace/config-linux.h\"" > ${S}/ace/config.h
    echo "include ${ACE_ROOT}/include/makeinclude/platform_linux.GNU" > ${PLATFORM_MACROS}
    echo "no_hidden_visibility ?= 1" >> ${PLATFORM_MACROS}
    
    cp ${WORKDIR}/ACE.mwc ${ACE_ROOT}
    
    cd ${ACE_ROOT}
    perl bin/mwc.pl -type automake ACE.mwc
    cd -
}

# TODO: dev package need to be fixed if wanted to be used later
FILES_${PN}-dev = "${libdir}/lib*.la \
${libdir}/pkgconfig \
${includedir}"

FILES_${PN} = "${libdir}/libACE_ETCL_Parser.so \
${libdir}/libACE_ETCL.so \
${libdir}/libACE_Monitor_Control.so \
${libdir}/libACE.so \
${libdir}/libACE_SSL.so \
${libdir}/libJAWS.so \
${libdir}/libKokyu.so \
${libdir}/libACE-${PV}.so \
${libdir}/libACE_ETCL-${PV}.so \
${libdir}/libACE_ETCL_Parser-${PV}.so \
${libdir}/libACE_Monitor_Control-${PV}.so \
${libdir}/libACE_SSL-${PV}.so \
${libdir}/libJAWS-${PV}.so \
${libdir}/libKokyu-${PV}.so \
"

INSANE_SKIP_${PN} = "dev-so"
