FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += " \
		file://client_template.conf \
		file://server_template.conf \
		file://openvpn-client.service \
		file://openvpn-server.service \
		"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

do_install_append() {
	# default configuration directory
	install -d 0755 ${D}${sysconfdir}/defaultconfig
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config

	# install default openvpn configuration directory
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config/openvpn

	# install default openvpn configuration
	install -m 0644 ${WORKDIR}/client_template.conf ${D}${sysconfdir}/defaultconfig/config/openvpn
	install -m 0644 ${WORKDIR}/server_template.conf ${D}${sysconfdir}/defaultconfig/config/openvpn

	# install openvpn configuration symlink
	rm -rf ${D}${sysconfdir}/openvpn
	ln -sf /mnt/rom/user/config/openvpn ${D}${sysconfdir}/openvpn

	# install openvpn services
	install -m 644 ${WORKDIR}/${PN}-client.service ${D}/${systemd_unitdir}/system
	install -m 644 ${WORKDIR}/${PN}-server.service ${D}/${systemd_unitdir}/system

	# enable systemd on sysinit
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
	ln -sf ../${PN}-client.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
	ln -sf ../${PN}-server.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
	# remove this dir as it won't be syncfs
	rmdir ${D}/${localstatedir}/run/openvpn
}

SYSTEMD_SERVICE_${PN} += " \
	${PN}-client.service \
	${PN}-server.service \
	"

FILES_${PN} += "${systemd_unitdir}"
