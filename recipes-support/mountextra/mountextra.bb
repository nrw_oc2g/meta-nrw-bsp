DESCRIPTION = "Extra mount options"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"

inherit systemd pkgconfig

SRC_URI += "file://mountextra.service \
            file://mountextra \
            "

S = "${WORKDIR}"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "mountextra.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "mountextra.service"

do_install() {
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/systemd
	install -d ${D}${sysconfdir}/systemd/system
	install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants
	install -d ${D}${bindir}
	install -m 0755 -d ${D}${base_libdir}
	install -m 0755 -d ${D}${systemd_unitdir}
	install -m 0755 -d ${D}${systemd_unitdir}/system
	install -m 0755 ${S}/mountextra ${D}${bindir}/mountextra
	install -m 0644 ${S}/mountextra.service ${D}${systemd_unitdir}/system/mountextra.service
	ln -sf ${systemd_unitdir}/system/mountextra.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/mountextra.service
}

FILES_${PN} += "${bindir} \
		${sysconfdir} \
		${systemd_unitdir}/* \
        "
