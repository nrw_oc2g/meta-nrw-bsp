inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "sudo_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI += "file://20-nonroot \
	   "

FILESEXTRAPATHS_append := "${THISDIR}/files:"

do_install_append() {
	install -m 0440 ${WORKDIR}/20-nonroot ${D}${sysconfdir}/sudoers.d/20-nonroot
}

FILES_${PN} += "\
	${sysconfdir}/sudoers.d/20-nonroot \
	"
