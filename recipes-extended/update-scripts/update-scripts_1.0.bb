SUMMARY = "Update scripts"
LICENSE = "CLOSED"
SECTION = "update scripts"

SRC_URI += "file://monolithicupdate \
file://syncfs \
file://update \
file://forcerecover \
file://monolithicupdate.sh \
file://syncfs.sh \
file://update.sh \
file://forcerecover.sh \
file://initsu \
file://initsu.sh \
file://analyzefs \
file://analyzefs.sh \
file://makefl \
file://makefl.sh \
"

S = "${WORKDIR}"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "update-scripts_1.0.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"


inherit allarch

RDEPENDS_${PN} += "rsync e2fsprogs-mke2fs e2fsprogs-e2fsck initscripts opkg wget"

do_install() {
     install -d ${D}${bindir}
     install -m 0755 ${S}/monolithicupdate ${D}${bindir}/.monolithicupdate
     install -m 0755 ${S}/syncfs ${D}${bindir}/.syncfs
     install -m 0755 ${S}/update ${D}${bindir}/.update
     install -m 0755 ${S}/forcerecover ${D}${bindir}/.forcerecover
     install -m 0755 ${S}/initsu ${D}${bindir}/.initsu
     install -m 0755 ${S}/analyzefs ${D}${bindir}/.analyzefs
     install -m 0755 ${S}/makefl ${D}${bindir}/.makefl
     install -m 0755 ${S}/monolithicupdate.sh ${D}${bindir}/monolithicupdate
     install -m 0755 ${S}/syncfs.sh ${D}${bindir}/syncfs
     install -m 0755 ${S}/update.sh ${D}${bindir}/update
     install -m 0755 ${S}/forcerecover.sh ${D}${bindir}/forcerecover
     install -m 0755 ${S}/initsu.sh ${D}${bindir}/initsu
     install -m 0755 ${S}/analyzefs.sh ${D}${bindir}/analyzefs
     install -m 0755 ${S}/makefl.sh ${D}${bindir}/makefl
}

FILES_${PN} += "${bindir}/monolithicupdate \
${bindir}/.monolithicupdate \
${bindir}/syncfs \
${bindir}/.syncfs \
${bindir}/forcerecover \
${bindir}/.forcerecover \
${bindir}/update \
${bindir}/.update \
${bindir}/initsu \
${bindir}/.initsu \
${bindir}/analyzefs \
${bindir}/.analyzefs \
${bindir}/makefl \
${bindir}/.makefl \
            "
