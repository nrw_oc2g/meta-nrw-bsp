#!/bin/sh
# Define this variable _NOSPINDLE before calling this script if this script run through a systemd service
# if this variable _SAFESYNC is also defined, it will do a wd safe sync instead (when watchdog daemon is not used)

STAG=syncfs:
PENDING=/mnt/storage/.updatealt_pending
MASTER=/dev/mmcblk0p2
ALT=/dev/mmcblk0p3
ENDSIG=/tmp/syncfs_sig
ERRSIG=/tmp/syncfs_err
PENDINGCHK=/tmp/.boot_pending
ONESYNCFSOVER=/tmp/.syncfs_over
# lock wait time max 15mn * 60 = 900 secs (sufficient time to update)
LOCKWAIT=900
# WARNING: lock must match monolithicupdate/update/forcerecover one
bname=updateperm
TMP=""
TMPBK=""
EXITSIG=/tmp/syncfs_exit_sig

sp="/-\|"
sc=0
spin() {
    if [ -z "$_NOSPINDLE" ]; then
        printf "\r${STAG} ${sp:sc++:1}"
        ((sc==${#sp})) && sc=0
    else
        printf "${STAG} ${sp:sc++:1} \n"
        ((sc==${#sp})) && sc=0
        if [ ! -z "$_SAFESYNC" ]; then
	    dmesg -D
	    echo 1 > /dev/watchdog
	    dmesg -E
	fi
    fi
}
endspin() {
    if [ -z "$_NOSPINDLE" ]; then
        printf "\r"
    else
        if [ ! -z "$_SAFESYNC" ]; then
	    dmesg -E
	fi
    fi
}

readonly LOCKFILE_DIR=/var/lock
readonly LOCK_FD=997

lock() {
    local prefix=$1
    local fd=${2:-$LOCK_FD}
    local lock_file=$LOCKFILE_DIR/$prefix.lock

    # still allow to run unlocked if the lock dir does not exist
    if [ ! -d "${LOCKFILE_DIR}" ]; then
	return 0
    fi

    # create lock file
    eval "exec $fd>$lock_file"
    # still allow to run unlocked if the lock file cannot be created
    if [ $? -ne 0 ]; then
	return 0
    fi

    # acquier the lock
    flock -w ${LOCKWAIT} $fd \
        && return 0 \
        || return 1
}

unlock() {
    local prefix=$1
    local fd=${2:-$LOCK_FD}
    local lock_file=$LOCKFILE_DIR/$prefix.lock

    # release the lock and removes the file
    flock -u $fd
    sync
}

function my_exit()
{
    if [ ! -f "$EXITSIG" ]; then
        touch "$EXITSIG"
        sync
        if [ -d "${TMP}" ]; then
            TMPMNT_RES=$(mount | grep "${TMP}")
	    TMPMNT_RET=$?
	    if [ $TMPMNT_RET -eq 0 ]; then
                umount ${TMP}
            fi
            rmdir "${TMP}"
        fi
        if [ -d "${TMPBK}" ]; then
            TMPBKMNT_RES=$(mount | grep "${TMPBK}")
	    TMPBKMNT_RET=$?
	    if [ $TMPBKMNT_RET -eq 0 ]; then
                umount ${TMPBK}
            fi
            rmdir "${TMPBK}"
        fi
        if [ -f "/tmp/syncfs-exclude" ]; then
            rm -f "/tmp/syncfs-exclude"
        fi
        unlock $bname
        exit $*
    else
        exit $*
    fi
}

function __sig_int {
    endspin
    echo "$STAG WARNING: SIGINT caught"
    my_exit 110
}

function __sig_quit {
    endspin
    echo "$STAG WARNING: SIGQUIT caught"
    my_exit 111
}

function __sig_term {
    endspin
    echo "$STAG WARNING: SIGTERM caught"
    my_exit 112
}

function __sig_hup {
    endspin
    echo "$STAG WARNING: SIGHUP caught"
    my_exit 113
}

# check if neither main/alt are mounted
checknopart() {
    if mount | grep "on / type ext4" | grep -q ${MASTER}; then
        return 1
    fi
    if mount | grep "on / type ext4" | grep -q ${ALT}; then
        return 1
    fi
    return 0
}

if [ "$#" -ne 1 ]; then
    echo "Usage: syncfs <DEVICE>"
    echo "example: syncfs ${ALT}"
    exit 1
fi

# Lock to test a single instance of update is running, and exit if wait timeout
echo "$STAG Checking if allowed to run..."
lock $bname || ( echo "$STAG Checking if allowed to run... failed"; exit 100 )
echo "$STAG Checking if allowed to run... done"

if [ ! -e "${ONESYNCFSOVER}" ]; then
    if [ -e "${PENDINGCHK}" ]; then
        echo "$STAG Pending reboot, could not run!"
        exit 97
    fi
else
    rm -f "${ONESYNCFSOVER}"
fi

if checknopart ; then
    echo "$STAG neither main/alt partition is rootfs, could not run"
    exit 102
elif mount | grep "on / type ext4" | grep -q mmcblk0p2; then
    # master partition
    if [ "$1" == "/dev/mmcblk0p2" ]; then
        echo "$STAG Cannot syncfs on current master partition!"
        exit 98
    fi
else
    # alt partition
    if [ "$1" == "/dev/mmcblk0p3" ]; then
        echo "$STAG Cannot syncfs on current backup partition!"
        exit 99
    fi
fi

rm -f ${EXITSIG}

# Set TRAPs to release lock if forced to exit
trap __sig_int SIGINT
trap __sig_quit SIGQUIT
trap __sig_term SIGTERM
trap __sig_hup SIGHUP

TMPBK=$(mktemp -d)
if [ $? -ne 0 ]; then
	echo "$STAG mktemp fail!"
	my_exit 58
fi

EXCLUDE="/dev/*
/proc/*
/sys/*
/tmp/*
/run/*
/media/*
/lost+found
/var/volatile/*
/var/lc15/*
/var/log/*
/var/tmp/*
/clean"

TMP=$(mktemp -d)
if [ $? -ne 0 ]; then
	echo "$STAG mktemp fail!"
	my_exit 50
fi

echo "$STAG Creating file system..."
mkfs.ext4 -F $1
if [ $? -ne 0 ]; then
	echo "$STAG Creating file system... fail!"
	my_exit 51
fi
echo "$STAG Creating file system... done"

if mount -o rw $1 $TMP; then
    cd /
    echo "${EXCLUDE}" > /tmp/syncfs-exclude

    # temporarily bind mount / to TMPBK
    mount --bind / ${TMPBK}
    if [ $? -ne 0 ]; then
        echo "$STAG mount rootfs to temp folder fail!"
	my_exit 52
    fi

    rm -f ${ENDSIG}
    rm -f ${ERRSIG}
    echo "$STAG Syncing files..."
    ( 
    rsync -aAXx --exclude-from=/tmp/syncfs-exclude ${TMPBK}/ ${TMP}
    if [ $? -ne 0 ]; then
        echo " $STAG rsync fail!"
        touch ${ERRSIG}
	exit 53
    fi
    touch ${ENDSIG}
    ) &
    until [ -f ${ENDSIG} ]; do
        spin
        if [ -f ${ERRSIG} ]; then
            my_exit 54
        fi
        if [ ! -f ${ENDSIG} ]; then
            sleep 1s
        fi
    done
    endspin
    echo "$STAG Syncing files... done"
    rm -f ${ENDSIG}
    rm -f ${ERRSIG}
    echo "$STAG System sync..."
    ( 
    sync
    if [ $? -ne 0 ]; then
        echo " $STAG sync fail!"
        touch ${ERRSIG}
	exit 55
    fi
    touch ${ENDSIG}
    ) &
    until [ -f ${ENDSIG} ]; do
        spin
        if [ -f ${ERRSIG} ]; then
            my_exit 56
        fi
        if [ ! -f ${ENDSIG} ]; then
            sleep 1s
        fi
    done
    endspin
    echo "$STAG System sync... done"
    
    touch ${TMP}/clean
    sync

    # Remove any possible pending update of backup partition if we manually just did it
    if [ "$1" == "${ALT}" ]; then
        rm -f ${PENDING}
        sync
    fi
else
    echo "$STAG Cannot mount $1"
    my_exit 57
fi

my_exit 0
