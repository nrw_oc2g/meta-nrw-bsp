#!/bin/sh
# Define this variable _NOSPINDLE before calling this script if this script run through a systemd service

TAG=monolithic-update:
altdevice=/dev/mmcblk0p3
storage=/mnt/storage
updatepackage=${storage}/.rootfs.tar.gz
checksumfile=${storage}/.rootfs.md5
ENDSIG=/tmp/monolithicupdate_sig
ERRSIG=/tmp/monolithicupdate_err
PENDING=${storage}/.updatealt_pending
PENDINGCHK=/tmp/.boot_pending
ONECHKBOOTOVER=/tmp/.checkboot_over
tempdir=""
exclude_list_file=""
TMPCONFIG=""
optionpackage=${storage}/.options.tar.gz
optioncheckfile=${storage}/.options.md5
optiondir=${storage}/.monooptions
DOOPTION=0

# lock wait time max 15mn * 60 = 900 secs (sufficient time to update)
LOCKWAIT=900
# WARNING: lock must match syncfs/update/forcerecover one
bname=updateperm

sp="/-\|"
sc=0
spin() {
    if [ -z "$_NOSPINDLE" ]; then
        printf "\r${TAG} ${sp:sc++:1}"
        ((sc==${#sp})) && sc=0
    else
        printf "${TAG} ${sp:sc++:1} \n"
        ((sc==${#sp})) && sc=0
    fi
}
endspin() {
    if [ -z "$_NOSPINDLE" ]; then
        printf "\r"
    fi
}

rebootalt() {
    # makes sure all bootable file are ok if not repair them
    touch ${ONECHKBOOTOVER}
    /usr/bin/checkboot
    FILE=$(find /sys -name bootcount)
    FWDTSTATE=$(find /sys -name wdt_hw_state)
    if [ -f "$FWDTSTATE" ] ; then
        wdtstate=$(cat $FWDTSTATE)
    else
        wdtstate=0
    fi
    echo 254 > ${FILE} && killall -9 watchdog && if [ $wdtstate -eq 1 ]; then halt; else log_write "$TAG Error wdt is not enabled, rebooting..." ; sleep 5s; reboot; fi
}

readonly LOCKFILE_DIR=/var/lock
readonly LOCK_FD=997

lock() {
    local prefix=$1
    local fd=${2:-$LOCK_FD}
    local lock_file=$LOCKFILE_DIR/$prefix.lock

    # still allow to run unlocked if the lock dir does not exist
    if [ ! -d "${LOCKFILE_DIR}" ]; then
	return 0
    fi

    # create lock file
    eval "exec $fd>$lock_file"
    # still allow to run unlocked if the lock file cannot be created
    if [ $? -ne 0 ]; then
	return 0
    fi

    # acquier the lock
    flock -w ${LOCKWAIT} $fd \
        && return 0 \
        || return 1
}

unlock() {
    local prefix=$1
    local fd=${2:-$LOCK_FD}
    local lock_file=$LOCKFILE_DIR/$prefix.lock

    # release the lock
    flock -u $fd
    sync
}

function log_write()
{
	echo "$*"
}

function log_write_nr()
{
	echo -n "$*"
}

function my_exit()
{
	trap - SIGINT
	trap - SIGQUIT
	trap - SIGTERM
	trap - SIGHUP
	sync
        if [ -f "${ENDSIG}" ]; then
            rm -f "${ENDSIG}"
        fi
        if mount | grep -q "${altdevice}"; then
	    umount "$altdevice"
	fi
        if [ -d "${tempdir}" ]; then
            rmdir "$tempdir"
        fi
        if [ -f "${exclude_list_file}" ]; then
            rm -f "${exclude_list_file}"
        fi
	if [ ! "$TMPCONFIG" == "" ] ; then
		rm -f "$TMPCONFIG"
	fi
        if [ -d "${optiondir}" ]; then
            rm -rf "$optiondir"
        fi
	cd "$_curr_dir"
	unlock $bname
	exit $*
}

function __sig_int {
    log_write "$TAG WARNING: SIGINT caught"
    my_exit 110
}

function __sig_quit {
    log_write "$TAG WARNING: SIGQUIT caught"
    my_exit 111
}

function __sig_term {
    log_write "$TAG WARNING: SIGTERM caught"
    my_exit 112
}

function __sig_hup {
    log_write "$TAG WARNING: SIGHUP caught"
    my_exit 113
}

function show_help {
    log_write "monolithicupdate help information:"
    log_write "Usage: monolithicupdate [[-b XXX] <PACKAGE_URL> <PACKAGE_CHKSUM_FILE_URL> [<OPT_URL> <OPT_CHKSUM_FILE_URL>]]"
    log_write "      options: -b XXX    # force download Bandwidth limit in XXX bytes/sec, append k or m"
    log_write "                         # suffix for XXXk kbytes/sec ot XXXm megabytes/sec"
    log_write "example: monolithicupdate http://192.168.33.64/test/update.tar.gz http://192.168.33.64/test/update.md5"
    log_write "example: monolithicupdate http://192.168.33.64/test/update.tar.gz http://192.168.33.64/test/update.md5 http://192.168.33.64/test/options.tar.gz http://192.168.33.64/test/options.md5"
    log_write "example when $updatepackage $checksumfile [$optionpackage $optioncheckfile] are local: monolithicupdate (all files must be own by root)"
}

function create_config {
    TMPCONFIG=$(mktemp)
    if [ $? -ne 0 ]; then
	    log_write "mktemp fail!"
	    return 70
    fi
    echo "limit_rate = $*" > "$TMPCONFIG"
    export WGETRC="$TMPCONFIG"
    return 0
}

function filecleanup {
    if [ -f "$updatepackage" ] ; then
        rm -f "$updatepackage"
    fi
    if [ -f "$checksumfile" ] ; then
        rm -f "$checksumfile"
    fi
    if [ -f "$optionpackage" ] ; then
        rm -f "$optionpackage"
    fi
    if [ -f "$optioncheckfile" ] ; then
        rm -f "$optioncheckfile"
    fi
}

# return 0 if provided file is own by root
function chkrootowner {
    if [ "Rroot" == "R$(stat -c %U $1)" ]; then
        return 0
    fi
    return 1
}

if [ -e "${PENDINGCHK}" ]; then
    log_write "$TAG Pending reboot, could not run!"
    exit 97
fi

# We must be in master partition to update or it won't work
log_write_nr "$TAG Verifying if current partition is master: "
if ! mount | grep "on / type ext4" | grep -q mmcblk0p2; then
    log_write "Failed!"
    exit 99
fi
log_write "Ok!"

_curr_dir=`pwd`

# Lock to test a single instance of update is running, and exit if wait timeout
log_write "$TAG Checking if allowed to run..."
lock $bname || ( log_write "$TAG Checking if allowed to run... failed"; exit 100 )
log_write "$TAG Checking if allowed to run... done"

# Set TRAPs to release lock if forced to exit
trap __sig_int SIGINT
trap __sig_quit SIGQUIT
trap __sig_term SIGTERM
trap __sig_hup SIGHUP

if [ -e "${PENDINGCHK}" ]; then
    log_write "$TAG Pending reboot, could not run!"
    my_exit 97
fi

TOTALARG=$#
while getopts :b:- FLAG; do
    case $FLAG in
        b)
        #log_write "$TAG Bandwidth limit option to $OPTARG bytes/sec (-b)"
        create_config $OPTARG
        if test $? -ne 0; then
                my_exit $?
        fi;;
        '-')
        show_help
        my_exit 0;;
        \?)
        log_write "Invalid option: -$OPTARG" && my_exit 1;;
        \:)
        log_write "Required argument not found for option: -$OPTARG" && my_exit 2;;
  esac
done

# removes processed option(s) from the cmd line args
shift $((OPTIND-1))

if [ "$#" -eq 2 ] || [ "$#" -eq 4 ]; then
    log_write "$TAG Get update package..."
    wget $1 -O $updatepackage
    if [ $? != 0 ]; then
        log_write "$TAG Get update package failed. Abort update process."
        filecleanup
        my_exit 3
    fi
    log_write "$TAG Get update package... done"
    log_write "$TAG Get checksum..."
    wget $2 -O $checksumfile
    if [ $? != 0 ]; then
        log_write "$TAG Get checksum failed. Abort update process."
        filecleanup
        my_exit 4
    fi
    log_write "$TAG Get checksum... done"
    name1=`basename $1`
    name2=`basename $updatepackage`
    sed -i "s/$name1/$name2/g" $checksumfile
    if [ "$#" -eq 4 ]; then
        log_write "$TAG Get options package..."
        wget $3 -O $optionpackage
        if [ $? != 0 ]; then
            log_write "$TAG Get options package failed. Abort update process."
            filecleanup
            my_exit 5
        fi
        log_write "$TAG Get options package... done"
        log_write "$TAG Get options checksum..."
        wget $4 -O $optioncheckfile
        if [ $? != 0 ]; then
            log_write "$TAG Get options checksum failed. Abort update process."
            filecleanup
            my_exit 6
        fi
        log_write "$TAG Get options checksum... done"
        name3=`basename $3`
        name4=`basename $optionpackage`
        sed -i "s/$name3/$name4/g" $optioncheckfile
        DOOPTION=1
    fi
elif [ "$TOTALARG" -eq 0 ]; then
    log_write "$TAG Resume monolithic update."
    if [ ! -e $updatepackage ] || [ ! -e $checksumfile ]; then
        log_write "$TAG Missing update package or checksum file."
        my_exit 7
    else
        chkrootowner $updatepackage
        if [ $? != 0 ]; then
            log_write "$TAG $updatepackage not own by root."
            filecleanup
            my_exit 8
        fi
        chkrootowner $checksumfile
        if [ $? != 0 ]; then
            log_write "$TAG $checksumfile not own by root."
            filecleanup
            my_exit 9
        fi
    fi 
    if [ -e $optionpackage ] && [ ! -e $optioncheckfile ]; then
        log_write "$TAG Missing options checksum file."
        my_exit 10
    fi
    if [ -e $optioncheckfile ] && [ ! -e $optionpackage ]; then
        log_write "$TAG Missing options package file."
        my_exit 11
    fi
    if [ -e $optionpackage ] && [ -e $optioncheckfile ]; then
        chkrootowner $optionpackage
        if [ $? != 0 ]; then
            log_write "$TAG $optionpackage not own by root."
            filecleanup
            my_exit 12
        fi
        chkrootowner $optioncheckfile
        if [ $? != 0 ]; then
            log_write "$TAG $optioncheckfile not own by root."
            filecleanup
            my_exit 13
        fi
        DOOPTION=1
    fi
else
    show_help
    my_exit 14
fi

# verify update image is valid
( cd $storage ; md5sum -c $checksumfile )
if [ $? != 0 ]; then
    log_write "$TAG Checksum verification failed. Abort update process."
    filecleanup
    my_exit 15
fi

if [ "$DOOPTION" -eq 1 ]; then
    # verify options image is valid
    ( cd $storage ; md5sum -c $optioncheckfile )
    if [ $? != 0 ]; then
        log_write "$TAG Options checksum verification failed. Abort update process."
        filecleanup
        my_exit 16
    fi
fi

# Remove any possible pending update of backup partition because we are updating it anyway...
if [ -e "${PENDING}" ] ; then
    rm -f ${PENDING}
    sync
fi

if [ "$DOOPTION" -eq 1 ]; then
    if [ -d "${optiondir}" ]; then
        rm -rf "$optiondir"
        if [ $? -ne 0 ]; then
            log_write "$TAG could not remove $optiondir directory!"
            my_exit 50
        fi
    fi
    myoptiondir=$(mkdir -p $optiondir)
    if [ $? -ne 0 ]; then
        log_write "$TAG could not create $optiondir directory!"
        my_exit 51
    fi
    rm -f ${ENDSIG}
    rm -f ${ERRSIG}
    log_write "$TAG Copying options files..."
    (
    tar -xf $optionpackage -C $optiondir/
    if [ $? -ne 0 ]; then
        log_write "$TAG tar $optionpackage fail!"
        touch ${ERRSIG}
        exit 52
    fi
    touch ${ENDSIG}
    ) &
    until [ -f ${ENDSIG} ]; do
        spin
        if [ -f ${ERRSIG} ]; then
            my_exit 53
        fi
        if [ ! -f ${ENDSIG} ]; then
            sleep 1s
        fi
    done
    endspin
    log_write "$TAG Copying options files... done"

    # call preinstall script if exist
    log_write "$TAG Checks if ($optiondir/preinstall.sh) exists"
    if [ -f "$optiondir/preinstall.sh" ] ; then
        log_write "$TAG Execute ($optiondir/preinstall.sh)..."
        # rootfs is available so 2nd param is 1
        $optiondir/preinstall.sh "$optiondir" "1" "/"
        if [ $? -ne 0 ]; then
            log_write "$TAG preinstall script fail!"
            filecleanup
            my_exit 54
        fi
        log_write "$TAG Execute ($optiondir/preinstall.sh)... done"
    fi
fi

log_write "$TAG Format $altdevice."
log_write "$TAG Creating file system..."
# makes sure that alt rootfs is not already mounted, if yes umount it
cd /
sync
umount $altdevice  > /dev/null 2>&1
sleep 1s
mkfs -t ext4 -F $altdevice
if [ $? -ne 0 ]; then
    log_write "$TAG Creating file system... fail!"
    my_exit 55
fi
log_write "$TAG Creating file system... done"

log_write "$TAG Write data to $altdevice."
tempdir=`mktemp -d`
if [ $? -ne 0 ]; then
    log_write "$TAG could not create tmp directory!"
    my_exit 56
fi
exclude_list_file=`mktemp`
if [ $? -ne 0 ]; then
    log_write "$TAG could not create tmp file!"
    my_exit 57
fi
echo "./clean" > $exclude_list_file
if [ $? -ne 0 ]; then
    log_write "$TAG could not write to $exclude_list_file!"
    my_exit 58
fi
mount $altdevice $tempdir
if [ $? -ne 0 ]; then
    log_write "$TAG mount $altdevice fail!"
    my_exit 59
fi
rm -f ${ENDSIG}
rm -f ${ERRSIG}
log_write "$TAG Copying files..."
(
tar -xf $updatepackage -C $tempdir/ -X $exclude_list_file
if [ $? -ne 0 ]; then
    log_write "$TAG tar $updatepackage fail!"
    touch ${ERRSIG}
    exit 60
fi
touch ${ENDSIG}
) &
until [ -f ${ENDSIG} ]; do
    spin
    if [ -f ${ERRSIG} ]; then
        my_exit 61
    fi
    if [ ! -f ${ENDSIG} ]; then
        sleep 1s
    fi
done
endspin
log_write "$TAG Copying files... done"
if [ "$DOOPTION" -eq 1 ]; then
    # call postinstall script if exist
    log_write "$TAG Checks if ($optiondir/postinstall.sh) exists"
    if [ -f "$optiondir/postinstall.sh" ] ; then
        log_write "$TAG Execute ($optiondir/postinstall.sh)..."
        # rootfs is available so 2nd param is 1
        $optiondir/postinstall.sh "$optiondir" "1" "$tempdir"
        if [ $? -ne 0 ]; then
            log_write "$TAG postinstall script fail!"
            my_exit 62
        fi
        log_write "$TAG Execute ($optiondir/postinstall.sh)... done"
    fi
fi
rm -f ${ENDSIG}
rm -f ${ERRSIG}
log_write "$TAG System sync..."
( 
sync
if [ $? -ne 0 ]; then
    log_write "$TAG sync fail!"
    touch ${ERRSIG}
    exit 63
fi
touch ${ENDSIG}
) &
until [ -f ${ENDSIG} ]; do
    spin
    if [ -f ${ERRSIG} ]; then
        my_exit 64
    fi
    if [ ! -f ${ENDSIG} ]; then
        sleep 1s
    fi
done
endspin
log_write "$TAG System sync... done"
touch $tempdir/clean
sync
umount $altdevice
rmdir $tempdir
rm $exclude_list_file
rm -f ${ENDSIG}
if [ -d "${optiondir}" ]; then
    rm -rf "$optiondir"
fi
sync

log_write "$TAG Reboot in $altdevice."
touch ${PENDINGCHK}
rebootalt
my_exit 0
