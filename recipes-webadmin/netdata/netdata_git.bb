HOMEPAGE = "https://github.com/firehol/netdata/"
SUMMARY = "Real-time performance monitoring"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE.md;md5=95b49e9ea979a337578f13c2a3ab9535"


SRC_URI = "git://github.com/firehol/netdata.git;protocol=https"

SRCREV = "8e3e6627ccd97959d64bbb4df1f377a39c0e753f"

# patch to disable timeout because timeout are not available with actual version
# of core-utils
SRC_URI += "file://0001-Correct-Timeout-issue.patch"

# patch to avoid creating a debian package which is not compatible anyway
SRC_URI += "file://0002-makefile-Do-not-build-contrib-dir.patch"

# patch for correctly printing to a pdf file
SRC_URI += "file://0003-corrects-print-to-pdf-path.patch"

# patch to add netdata bsp related groups
SRC_URI += "file://0004-add-bsp-groups.patch"

# patch to add thermal sensor reading
SRC_URI += "file://0005-thermal-sensors-python.patch"
SRC_URI += "file://0006-thermal-sensors-charts.patch"

# default netdata.conf for netdata configuration
SRC_URI += "file://netdata.conf"

# file for providing systemd service support
SRC_URI += "file://netdata.service"

S = "${WORKDIR}/git"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "netdata_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}" 
PKGV = "${PKGGITV}"

DEPENDS += "zlib util-linux libcap"

inherit pkgconfig autotools useradd systemd

#systemd
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "netdata.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

#User specific
USERADD_PACKAGES = "${PN}"
GROUPADD_PARAM_${PN} = "--system netdata"

hostname ?= "netdata"
history ?= "86400"
update_every ?= "1"
cachedir ?= "${localstatedir}/cache"

do_install_append() {
    #set S UID for plugins
    chmod 4755 ${D}${libexecdir}/netdata/plugins.d/apps.plugin

    if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
        # Install systemd unit files
        install -d ${D}${systemd_unitdir}/system
        install -m 0644 ${WORKDIR}/netdata.service ${D}${systemd_unitdir}/system
        sed -i -e 's,@@datadir,${datadir_native},g' ${D}${systemd_unitdir}/system/netdata.service
        sed -i -e 's,@@cachedir,${cachedir},g' ${D}${systemd_unitdir}/system/netdata.service
    fi

    # Install default netdata.conf
    install -d ${D}${sysconfdir}/netdata
    install -m 0644 ${WORKDIR}/netdata.conf ${D}${sysconfdir}/netdata/
    sed -i -e 's,@@hostname,${hostname},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@history,${history},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@update_every,${update_every},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@sysconfdir,${sysconfdir},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@libdir,${libexecdir},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@datadir,${datadir},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@cachedir,${cachedir},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@runmode,${runmode},g' ${D}${sysconfdir}/netdata/netdata.conf
    sed -i -e 's,@@srunmodecache,${srunmodecache},g' ${D}${systemd_unitdir}/system/netdata.service
    sed -i -e 's,@@srunmodeowncache,${srunmodeowncache},g' ${D}${systemd_unitdir}/system/netdata.service
}

FILES_${PN}-dbg += "${libexecdir}/netdata/plugins.d/.debug"
RDEPENDS_${PN} = "bash zlib"
