inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "webmin_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

# makes sure webmin starts before watchdog
INITSCRIPT_PARAMS = "start 94 5 3 2 . stop 10 0 1 6 ."

WEBMIN_LOGIN = "admin"
WEBMIN_PASSWORD = "nuranwireless"

PACKAGES += " \
	${PN}-module-ajaxterm \
	${PN}-module-cron \
	${PN}-module-custom \
	${PN}-module-filemin \
	${PN}-module-mount \
	${PN}-module-net \
	${PN}-module-passwd \
	${PN}-module-proc \
	${PN}-module-servers \
	${PN}-module-sshd \
	${PN}-module-status \
	${PN}-module-time \
	${PN}-module-useradmin \
	${PN}-module-webmincron \
"

do_install_prepend() {
	export allow="127.0.0.1"
	export webprefix="/admin"
}

do_install_append() {
	# Setup for runtime webmin password
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	install -d ${D}${sysconfdir}/defaultconfig/config/webmin
	install -m 0600 ${D}${sysconfdir}/webmin/miniserv.users ${D}${sysconfdir}/defaultconfig/config/webmin/miniserv.users
	rm -rf ${D}${sysconfdir}/webmin/miniserv.users
	ln -sf /mnt/rom/user/config/webmin/miniserv.users ${D}${sysconfdir}/webmin/miniserv.users
}

FILES_${PN} += "${sysconfdir}/defaultconfig/config/webmin/miniserv.users \
        "