FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "e2fsprogs_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI += "file://e2fsck.conf"

# install custom e2fsck.conf
do_install_append() {
    install -d ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/e2fsck.conf ${D}${sysconfdir}
}

FILES_e2fsprogs-e2fsck += "${sysconfdir}/e2fsck.conf"
