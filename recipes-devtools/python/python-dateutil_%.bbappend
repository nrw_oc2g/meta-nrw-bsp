inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "python-dateutil_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

CLEANBROKEN = "1"
