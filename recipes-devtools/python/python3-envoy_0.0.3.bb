DESCRIPTION = "Simple API for running external processes."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "548cc52576b6d73fa886439e3100d576"
SRC_URI[sha256sum] = "22b02009cfda2cf2cdb94a75a15ac3fd910aea8685c53e8e03715c7e9d8e8bde"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_${PV}.bb"
PR       := "r${REPOGITFN}"

inherit pypi setuptools3
