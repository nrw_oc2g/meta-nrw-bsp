DESCRIPTION = "Various helpers to pass trusted data to untrusted environments"
HOMEPAGE = "https://github.com/pallets/itsdangerous"
SECTION = "devel/python"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

S = "${WORKDIR}/git"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

BRANCH = "master"
SRCREV = "0.22"
SRC_URI = "git://github.com/pallets/itsdangerous.git;branch=${BRANCH}"


inherit setuptools3

do_configure_preprend() {
	rm ${S}/Makefile
}

CLEANBROKEN = "1"
