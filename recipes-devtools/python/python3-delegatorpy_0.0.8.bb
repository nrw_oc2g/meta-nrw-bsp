DESCRIPTION = "simple library for dealing with subprocesses"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "32207a6dc5dee99d2fdb5345dfb3b366"
SRC_URI[sha256sum] = "603c1c1c76b1340520d95a1e768cf2d3a8df7b3c15a1eb9df83aac29b8d025a4"

PYPI_PACKAGE = "delegator.py"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_${PV}.bb"
PR       := "r${REPOGITFN}"

inherit pypi setuptools3
