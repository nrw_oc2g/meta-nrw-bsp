FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += "file://opkg-setup.service "

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "opkg_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

do_install_append() {
	install -d 0755 ${D}${sysconfdir}
	install -d 0755 ${D}${sysconfdir}/defaultconfig
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config/opkg
	install -d 0755 ${D}${sysconfdir}/systemd
	install -d 0755 ${D}${sysconfdir}/systemd/system
	install -d 0755 ${D}${libdir}
	install -d 0755 ${D}${systemd_unitdir}
	install -d 0755 ${D}${systemd_unitdir}/system
	install -d 0755 ${D}${systemd_unitdir}/system/multi-user.target.wants
	install -d 0755 ${D}${localstatedir}
	install -d 0755 ${D}/mnt
	install -d 0755 ${D}/mnt/rom
	install -d 0755 ${D}/mnt/rom/user
	# Setup for runtime opkg configuration
	install -m 0644 ${D}${sysconfdir}/opkg/opkg.conf ${D}${sysconfdir}/defaultconfig/config/opkg/opkg.conf
	rm -rf ${D}${sysconfdir}/opkg/opkg.conf
	ln -sf /mnt/rom/user/config/opkg/opkg.conf ${D}${sysconfdir}/opkg/opkg.conf
	#disable opkg 1st boot service
	ln -s /dev/null ${D}${sysconfdir}/systemd/system/opkg-configure.service
	# Setup for runtime
	install -m 0644 ${WORKDIR}/opkg-setup.service ${D}${systemd_unitdir}/system/
	ln -sf ${systemd_unitdir}/system/opkg-setup.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
	rm -rf ${D}${sysconfdir}/systemd/system/opkg-configure.service
	rm -rf ${D}${systemd_unitdir}/system/opkg-configure.service
}

SYSTEMD_SERVICE_${PN} = ""

FILES_${PN} += " \
	${sysconfdir} \
	${sysconfdir}/defaultconfig \
	${sysconfdir}/defaultconfig/config \
	${sysconfdir}/defaultconfig/config/opkg/* \
	${sysconfdir}/systemd \
	${sysconfdir}/systemd/system/* \
	${systemd_unitdir}/systemd \
	${systemd_unitdir}/systemd/system/* \
	/mnt \
	/mnt/rom \
	/mnt/rom/user \
"
