SUMMARY = "Simple shell based CA utility"
HOMEPAGE = "https://github.com/OpenVPN/easy-rsa"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=e944ef975ef9d0312e63c9ee80df17fc"

inherit autotools

S = "${WORKDIR}/git"

BRANCH = "release/2.x"
SRCREV = "${AUTOREV}"

SRC_URI = "git://github.com/OpenVPN/easy-rsa.git;branch=${BRANCH}"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

do_install_append() {
	# default user's directory
	install -d 0755 ${D}/home
	install -d 0755 ${D}/home/root
	install -d 0755 ${D}/home/gsm

	# this removes the error message easy-rsa will generate when ran with a ro rootfs
	ln -s /tmp/.rnd ${D}/home/root/.rnd
	ln -s /tmp/.rnd ${D}/home/gsm/.rnd
}

FILES_${PN} += "/home/root/.rnd \
		/home/gsm/.rnd"
