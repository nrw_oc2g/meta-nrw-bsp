DESCRIPTION = "Non-interactive ssh password auth"
HOMEPAGE = "https://sourceforge.net/projects/sshpass/"
SECTION = "console/network"
LICENSE = "GPLv2"

SRC_URI = "http://sourceforge.net/projects/sshpass/files/sshpass/${PV}/sshpass-${PV}.tar.gz"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "sshpass.inc"
INC_PR   := "r${REPOGITFN}"

inherit autotools
