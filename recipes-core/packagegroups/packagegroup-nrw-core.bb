DESCRIPTION = "NuranWireless default Package Groups"
LICENSE = "MIT"

inherit packagegroup

PACKAGES = "\
	packagegroup-nrw-extra \
	"

RDEPENDS_packagegroup-nrw-extra = "\
	cpufrequtils \
	lc15-firmware \
	gpsd \
	packagegroup-core-ssh-openssh \
	openssh-sftp \
	devmem2 \
	pps-tools \
	screen \
	ethtool \
	util-linux \
	lmsensors-sensors \
	lmsensors-sensord \
	mtd-utils \
	mtd-utils-jffs2 \
	mtd-utils-misc \
	nginx \
	initscripts \
	webmin \
	lc15-sysdev-remap \
	libgsm \
	libace \
	cronie \
	python-dateutil \
	python-modules \
	python-pexpect \
	watchdog \
	sysstat \
	ntp \
	ntpdate \
	ntp-utils \
	tzdata \
	gettime \
	u-boot-tools \
	u-boot-fw-utils \
	u-boot \
	e2fsprogs \
	update-scripts \
	repair \
	tcpdump \
	valgrind \
	libtalloc \
	i2c-tools \
	wget \
	sshpass \
	iproute2 \
	iproute2-tc \
	iperf3 \
	lsof \
	procps \
	logrotate \
	netdata \
	curl \
	python3 \
	dosfstools \
	openvpn \
	easy-rsa \
	nfs-utils-client \
	python-dbus \
	python-pygobject \
	mountextra \
	sudo \
	gps-utils \
	backup-scripts \
	iotop \
	perl-module-version \
	perl-module-version-regex \
	perl-module-overloading \
	perl-module-overload \
	perl-module-overload-numbers \
	perl-module-text-parsewords \
	perl-module-pod-usage \
	perl-module-getopt-long \
	perl-module-getopt-std \
	perl-module-mro \
	e2fsprogs-tune2fs \
	loadproc \
	glibc-utils \
	glib-2.0-utils \
	strongswan \
	"
