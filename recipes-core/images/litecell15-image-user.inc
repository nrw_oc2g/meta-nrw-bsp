LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

# generated file must match analyzefs tool file list image format
install_clean_and_list() {
	# Cleanup and create the list of rootfs files
	rm -f ${IMAGE_ROOTFS}/.lock
	touch ${IMAGE_ROOTFS}/clean
	rm -f ${IMAGE_ROOTFS}/.flst
	{ cd ${IMAGE_ROOTFS} && find .; } | LC_ALL=C sort > ${TMPDIR}/myfilelst
	{ cd ${IMAGE_ROOTFS}; while read LINE; do if [ -d $LINE ] && [ ! -h $LINE ] && [ ! -L $LINE ]; then MYDIR="0"; else MYDIR="%s"; fi; LC_ALL=C stat --printf "%N,$MYDIR,%F,%a,%u,%g\n" $LINE; if [ -f $LINE ] && [ ! -h $LINE ] && [ ! -L $LINE ]; then md5sum $LINE ; fi; done < ${TMPDIR}/myfilelst ;} > ${IMAGE_ROOTFS}/.flst
	chmod 400 ${IMAGE_ROOTFS}/.flst
	rm -f ${TMPDIR}/myfilelst
}

# reinstall modified shadow package (remove from rootfs before ROOTFS_POSTUNINSTALL_COMMAND) now ro compatible,
# as it would be installed anyway if package update of many packages is made from the target
# shadow package is in dependency of lots of package...
install_myextra() {
	${STAGING_BINDIR_NATIVE}/opkg --volatile-cache -f ${WORKDIR}/opkg.conf -o ${IMAGE_ROOTFS} --force_postinstall --prefer-arch-to-version install shadow
}

IMAGE_PREPROCESS_COMMAND += "install_clean_and_list; "
ROOTFS_POSTUNINSTALL_COMMAND =+ "install_myextra; "
