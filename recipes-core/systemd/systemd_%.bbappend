FILESEXTRAPATHS_prepend := "${THISDIR}/systemd:"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "systemd_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI += "file://wired.network \
	file://static1.network \
	file://0024-wait-online-only-eth0.patch \
	file://0025-no-epoc-time-validation.patch \
    "

PACKAGECONFIG_append = " networkd resolved"

PACKAGECONFIG[timedated] = "--disable-timedated,--disable-timedated"
PACKAGECONFIG[timesyncd] = "--disable-timesyncd,--disable-timesyncd"
PACKAGECONFIG[logind] = "--disable-logind,--disable-logind"

# do not recreate mtab and localtime symlinks
do_configure_prepend() {
    sed -i '/\/etc\/mtab/d' ${S}/tmpfiles.d/etc.conf.m4
    sed -i '/\/etc\/localtime/d' ${S}/tmpfiles.d/etc.conf.m4
}

do_install_append() {
    install -d ${D}${sysconfdir}
    install -d ${D}${sysconfdir}/defaultconfig
    install -d ${D}${sysconfdir}/defaultconfig/config
    install -d ${D}${sysconfdir}/defaultconfig/config/network
    install -m 0644 ${WORKDIR}/wired.network ${D}${sysconfdir}/defaultconfig/config/network/wired.network
    install -m 0644 ${WORKDIR}/static1.network ${D}${sysconfdir}/defaultconfig/config/network/static1.network
    rmdir ${D}${sysconfdir}/systemd/network
    ln -sf /mnt/rom/user/config/network ${D}${sysconfdir}/systemd/network

    cd ${D}${sysconfdir}
    ln -sf /run/systemd/resolve/resolv.conf resolv.conf
    cd -
    rm -f ${D}${sysconfdir}/systemd/system/systemd-timesyncd.service
    ln -s /dev/null ${D}${sysconfdir}/systemd/system/systemd-timesyncd.service
    rm -f ${D}${sysconfdir}/systemd/system/systemd-timedated.service
    ln -s /dev/null ${D}${sysconfdir}/systemd/system/systemd-timedated.service
    rm -f ${D}${bindir}/timedatectl
    rm -f ${D}${sysconfdir}/systemd/system/systemd-hostnamed.service
    ln -s /dev/null ${D}${sysconfdir}/systemd/system/systemd-hostnamed.service
    rm -f ${D}${bindir}/hostnamectl
    rm -f ${D}${sysconfdir}/systemd/system/hostname.service
    rm -f ${D}${sysconfdir}/systemd/system/systemd-localed.service
    ln -s /dev/null ${D}${sysconfdir}/systemd/system/systemd-localed.service
    rm -f ${D}${bindir}/localectl
    sed -i '/RuntimeMaxUse/d' ${D}${sysconfdir}/systemd/journald.conf
    echo "RuntimeMaxUse=8M" >> ${D}${sysconfdir}/systemd/journald.conf
    sed -i '/RuntimeKeepFree/d' ${D}${sysconfdir}/systemd/journald.conf
    echo "RuntimeKeepFree=16M" >> ${D}${sysconfdir}/systemd/journald.conf
    sed -i '/RuntimeMaxFileSize/d' ${D}${sysconfdir}/systemd/journald.conf
    echo "RuntimeMaxFileSize=500K" >> ${D}${sysconfdir}/systemd/journald.conf
    # makes sure tmp cleanup service is not run as it may conflict with the BSP
    rm -f ${D}${systemd_unitdir}/system/systemd-tmpfiles-clean.timer
    rm -f ${D}${systemd_unitdir}/system/timers.target.wants/systemd-tmpfiles-clean.timer
    # makes sure systemd-logind is not used
	rm -f ${D}${sysconfdir}/systemd/system/systemd-logind.service
    ln -s /dev/null ${D}${sysconfdir}/systemd/system/systemd-logind.service
	rm -f ${D}${systemd_unitdir}/system/multi-user.target.wants/systemd-logind.service
}

# if watchdog daemon service is active, we need to kill it before installing systemd
pkg_prerm_${PN}_append () {
if type systemctl >/dev/null 2>/dev/null; then
	_MYSTAT=$(killall -9 watchdog)
fi

}

# check the expected content for every systemd version post 229
pkg_postinst_${PN} () {
	sed -e '/^hosts:/s/\s*\<myhostname\>//' \
		-e 's/\(^hosts:.*\)\(\<files\>\)\(.*\)\(\<dns\>\)\(.*\)/\1\2 myhostname \3\4\5/' \
		-i $D/etc/nsswitch.conf

	update-alternatives --install /sbin/init init /lib/systemd/systemd 300
	update-alternatives --install /sbin/halt halt /bin/systemctl 300
	update-alternatives --install /sbin/reboot reboot /bin/systemctl 300
	update-alternatives --install /sbin/shutdown shutdown /bin/systemctl 300
	update-alternatives --install /sbin/poweroff poweroff /bin/systemctl 300
	update-alternatives --install /sbin/runlevel runlevel /bin/systemctl 300

OPTS=""

if [ -n "$D" ]; then
    OPTS="--root=$D"
fi

if type systemctl >/dev/null 2>/dev/null; then
	kill 1
	sleep 1s
	_MYSTAT=$(systemctl $OPTS is-enabled watchdog.sh)

	if [[ "$_MYSTAT" =~ "enabled" ]]; then
		if [ -z "$D" ]; then
			systemctl stop watchdog.service
			sleep 1s
			systemctl start watchdog.service
		fi
	fi
fi
exit $?

}

FILES_${PN} += "/etc/resolv.conf \
		${sysconfdir} \
		${sysconfdir}/defaultconfig/* \
		"

INSANE_SKIP_${PN} += "debug-files installed-vs-shipped"
