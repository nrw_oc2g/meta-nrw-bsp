FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "initscripts_1.0.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI += "file://checkroot.sh \
	file://checkstoragefs.sh \
	file://checkmonolithicupdate.sh \
	file://checkroot.service \
	file://checkstoragefs.service \
	file://checkmonolithicupdate.service \
"

RDEPENDS_${PN} += "rsync e2fsprogs-mke2fs e2fsprogs-e2fsck update-scripts base-files"

do_install_append() {
	install -m 0755 ${S}/checkstoragefs.sh ${D}${sysconfdir}/init.d
	install -m 0755 ${S}/checkmonolithicupdate.sh ${D}${sysconfdir}/init.d
	update-rc.d -r ${D} checkstoragefs.sh start 07 S .
	update-rc.d -r ${D} checkmonolithicupdate.sh start 99 S .
	# Create system related directory for services below
	install -d 0755 ${D}${sysconfdir}/systemd
	install -d 0755 ${D}${base_libdir}/systemd/system
	install -d 0755 ${D}${sysconfdir}/systemd/system/multi-user.target.wants
	# Add checkroot service
	install -m 0644 ${S}/checkroot.service ${D}${base_libdir}/systemd/system/checkroot.service
	ln -sf ${base_libdir}/systemd/system/checkroot.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/checkroot.service
	# Add checkstoragefs service
	install -m 0644 ${S}/checkstoragefs.service ${D}${base_libdir}/systemd/system/checkstoragefs.service
	ln -sf ${base_libdir}/systemd/system/checkstoragefs.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/checkstoragefs.service
	# Add checkmonolithicupdate service
	install -m 0644 ${S}/checkmonolithicupdate.service ${D}${base_libdir}/systemd/system/checkmonolithicupdate.service
	ln -sf ${base_libdir}/systemd/system/checkmonolithicupdate.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/checkmonolithicupdate.service
}

FILES_${PN} += "${bindir} \
	${sysconfdir} \
	${base_libdir} \
"

MASKED_SCRIPTS = " \
	banner \
	bootmisc \
	checkfs \
	devpts \
	dmesg \
	mountall \
	mountnfs \
	populate-volatile \
	read-only-rootfs-hook \
	rmnologin \
	sysfs \
	urandom \
"

