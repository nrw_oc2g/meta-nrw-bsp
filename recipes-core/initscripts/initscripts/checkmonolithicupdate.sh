#!/bin/sh

export _NOSPINDLE=1

masterdev=/dev/mmcblk0p2
altdev=/dev/mmcblk0p3
storage=/mnt/storage
updatepackage=${storage}/.rootfs.tar.gz
checksumfile=${storage}/.rootfs.md5
PENDINGCHK=/tmp/.boot_pending
ONESYNCFSOVER=/tmp/.syncfs_over
ONECHKBOOTOVER=/tmp/.checkboot_over
# next definition must also match the one in update
UPDATELOG=${storage}/update.log
optionpackage=${storage}/.options.tar.gz
optioncheckfile=${storage}/.options.md5

safereboot() {
    # makes sure all bootable file are ok, if not repair/upgrade them
    echo "Verify boot file possible update..."
    touch ${ONECHKBOOTOVER}
    /usr/bin/checkboot -c -d
    __CHECK_RET=$?
    if test ${__CHECK_RET} -eq 100; then
        sleep 30s
        touch ${ONECHKBOOTOVER}
        /usr/bin/checkboot -c -d
        __CHECK_RET=$?
    fi
    if test ${__CHECK_RET} -ne 0; then
        echo "Verify boot file possible update... error!"
    else
        echo "Verify boot file possible update... done!"
    fi
    echo "Reboot..."
    reboot
}

# check if neither main/alt are mounted
checknopart() {
    if mount | grep "on / type ext4" | grep -q ${masterdev}; then
        return 1
    fi
    if mount | grep "on / type ext4" | grep -q ${altdev}; then
        return 1
    fi
    return 0
}

function filecleanup {
    if [ -f "$updatepackage" ] ; then
        rm -f "$updatepackage"
    fi
    if [ -f "$checksumfile" ] ; then
        rm -f "$checksumfile"
    fi
    if [ -f "$optionpackage" ] ; then
        rm -f "$optionpackage"
    fi
    if [ -f "$optioncheckfile" ] ; then
        rm -f "$optioncheckfile"
    fi
}

# makes sure we don't attempt any update if possible rootfs repair(reboot) indicated by PENDINGCHK is pending to avoid further corruption
if [ ! -e "${PENDINGCHK}" ]; then
# makes sure this file does not exist (normally not) to prevent any interference in other check... scripts
rm -f ${ONESYNCFSOVER}
if [ -e "/clean" ] && [ -e "$updatepackage" ]; then
    if checknopart ; then
        echo "Not using main/alt rootfs, update suspended..."
    elif mount | grep "on / type ext4" | grep -q mmcblk0p2 ; then
        #Wait for watchdog to start before running monolithicupdate which will kill the watchdog to reboot on alt.
        (
        echo "Previous update failed during process. Waiting for watchdog process..."
        WDTEST_RET=1
        until [ $WDTEST_RET -eq 0 ]; do
		WDTEST=$(ps aux | grep "watchdog" | grep "/usr/sbin/")
		WDTEST_RET=$?
		if test ${WDTEST_RET} -ne 0; then
			sleep 15s
		fi
        done
        echo "Previous monolithicupdate failed during process. Restart process."
        monolithicupdate
        ) &
    else
        echo "Current partition has been updated. Wait 2m before syncing partitions."
        # prevent any further update to happen because of possible corruption since we will reboot in background
        touch ${PENDINGCHK}
        (
        sleep 2m
        echo "Current partition OK. Remove /clean in $masterdev and start syncing."
        tempdir=`mktemp -d`
        mount $masterdev $tempdir
        rm $tempdir/clean
        sync
        umount $masterdev
        rmdir $tempdir
        filecleanup
        sync
        # force syncfs which won't happen if called manually because of PENDINGCHK protection
        touch ${ONESYNCFSOVER}
        syncfs $masterdev
        rm ${UPDATELOG}
        sync

        echo "Syncing ended."
        safereboot
        ) &
    fi
fi
else
    echo "Pending reboot, could not run!"
fi

