#!/usr/bin/env python

##
# Adds a signal handler to the 'startup finished' signal on systemd.
#
# We then re-execute the default systemd target so it activates the systemd
# changes made in flash during boot by the user.
##

import dbus
from gi.repository import GObject
from dbus.mainloop.glib import DBusGMainLoop

import logging
import subprocess

logging.basicConfig(
    filename='/var/log/systemdcust.log',
    level=logging.DEBUG,
    format='%(asctime)s [%(name)s][%(levelname)s] %(message)s'
)

def startup_finished_handler(firmware, loader, kernel, initrd, userspace, total):
    try:
        logging.info('systemd "startup finished" signal detected. Re-activating default.target...')

        logging.info('calling systemctl daemon-reload...')
        p = subprocess.Popen(['/bin/systemctl', 'daemon-reload'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        logging.info('systemctl daemon-reload completed.')
        if stdout:
            logging.info('systemctl daemon-reload stdout:\n' + stdout)
        if stderr:
            logging.warning('systemctl daemon-reload stderr:\n' + stderr)

        logging.info('calling systemctl start default.target...')
        p = subprocess.Popen(['/bin/systemctl', 'start', 'default.target'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        logging.info('systemctl start default.target completed.')
        if stdout:
            logging.info('systemctl start default.target stdout:\n' + stdout)
        if stderr:
            logging.warning('systemctl start default.target stderr:\n' + stderr)

        # handler completed; quit now
        logging.info('handler finished, quitting event loop.')
        loop.quit()

    except:
        logging.exception('Exception occurred in startup_finished handler.')

# hook up the handler
dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

bus = dbus.SystemBus()
systemd = bus.get_object('org.freedesktop.systemd1','/org/freedesktop/systemd1')
manager = dbus.Interface(systemd, dbus_interface='org.freedesktop.systemd1.Manager')
manager.Subscribe()

bus.add_signal_receiver(
    startup_finished_handler,
    dbus_interface='org.freedesktop.systemd1.Manager',
    signal_name='StartupFinished'
)

# start the event loop
logging.info('Starting the DBus event loop...')
loop = GObject.MainLoop()
loop.run()
