SUMMARY = "U-Boot maintenance scripts"
LICENSE = "CLOSED"
SECTION = "bootloader"

SRC_URI = "file://ubdump \
file://ubrestore \
file://ubvalidate \
file://mlovalidate \
file://mlorestore \
file://mlodump \
"

RDEPENDS_${PN} = "u-boot-mkimage mtd-utils busybox util-linux coreutils"

S = "${WORKDIR}"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "u-boot-tools_0.1.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

inherit allarch

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${S}/ubdump ${D}${bindir}/ubdump
	install -m 0755 ${S}/ubrestore ${D}${bindir}/ubrestore
	install -m 0755 ${S}/ubvalidate ${D}${bindir}/ubvalidate
	install -m 0755 ${S}/mlovalidate ${D}${bindir}/mlovalidate
	install -m 0755 ${S}/mlorestore ${D}${bindir}/mlorestore
	install -m 0755 ${S}/mlodump ${D}${bindir}/mlodump
}
