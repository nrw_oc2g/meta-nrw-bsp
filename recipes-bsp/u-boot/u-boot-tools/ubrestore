#!/bin/sh

SRC="$1"
DST="$2"

if [ $# -ne 2 ]; then
    echo "Usage: ubrestore <src> <dest>"
    echo ""
    echo "Example: ubrestore /dev/mtdblock4 /dev/mtd7"
    echo "         ubrestore uboot.bin /dev/mtd7"
fi

if ! echo ${DST} | grep -q "mtd[0-9]"; then
    echo >&2 "Error: use the mtd device"
    exit 1
fi

TMP=${SRC}
if [ ! -f ${SRC} ]; then
    TMP=$(mktemp)
    if ! ubdump ${SRC} ${TMP}; then
        rm -f ${TMP}
        exit 2
    fi
fi

# double check the input file to make sure it is ok before using it for restore
__MUBFILE_RES=$(/usr/bin/ubvalidate --f ${TMP} 2>&1)
__MUBFILE_RET=$?
if test ${__MUBFILE_RET} -ne 0; then
    echo "$__MUBFILE_RES"
	echo "Error: '${SRC}' content is invalid (cannot be used for restore)."
    if [ ! -f ${SRC} ]; then
        rm -f ${TMP}
    fi
    exit 3
fi

RET=0
if ! flashcp -v ${TMP} ${DST}; then
    RET=4
fi

if [ ! -f ${SRC} ]; then
    rm -f ${TMP}
fi

exit $RET
