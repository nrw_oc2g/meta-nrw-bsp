FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
FILESPATHBASE = "${THISDIR}/${PN}/gpsd:"

inherit gitver-repo

SRC_URI += " \
    file://0001-limited-hunt-baudrate.patch \
"

REPODIR   = "${THISDIR}"
REPOFILE  = "gpsd_3.%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/gpsd"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

# GPS protocols
EXTRA_OECONF =+ "nmea0183='true'"
EXTRA_OECONF =+ "ashtech='false'"
EXTRA_OECONF =+ "earthmate='false'"
EXTRA_OECONF =+ "evermore='false'"
EXTRA_OECONF =+ "fv18='false'"
EXTRA_OECONF =+ "garmin='false'"
EXTRA_OECONF =+ "garmintxt='false'"
EXTRA_OECONF =+ "geostar='false'"
EXTRA_OECONF =+ "itrax='false'"
EXTRA_OECONF =+ "mtk3301='true'"
EXTRA_OECONF =+ "navcom='false'"
EXTRA_OECONF =+ "oncore='false'"
EXTRA_OECONF =+ "sirf='false'"
EXTRA_OECONF =+ "superstar2='false'"
EXTRA_OECONF =+ "tnt='false'"
EXTRA_OECONF =+ "tripmate='false'"
EXTRA_OECONF =+ "tsip='false'"
EXTRA_OECONF =+ "ublox='false'"
EXTRA_OECONF =+ "fury='false'"
EXTRA_OECONF =+ "nmea2000='false'"
# Non-GPS protocols
EXTRA_OECONF =+ "gpsclock='false'"
# Time service
EXTRA_OECONF =+ "pps='true'"
# Communication
EXTRA_OECONF =+ "usb='false'"
EXTRA_OECONF =+ "bluez='false'"

