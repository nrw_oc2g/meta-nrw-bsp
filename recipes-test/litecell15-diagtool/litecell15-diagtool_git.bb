SUMMARY = "LiteCell 1.5 Test Application (Diag)"
LICENSE = "CLOSED"

NRW_INTERNAL_MIRROR ??= ""

S = "${WORKDIR}/git"

DEPENDS = "lc15-firmware"

inherit gitver-pkg gitver-repo 

REPODIR   = "${THISDIR}"
REPOFILE  = "litecell15-diagtool_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}" 
PKGV = "${PKGGITV}"
    
DEV_SRCREV  = "${AUTOREV}"
REL_SRCREV  = "77a5826d1563c73e9e877fc2d391b30d3c31ce56"
SRCREV      = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BSP_DEVEL', False) else '${REL_SRCREV}'}"

BRANCH   = "${@ 'nrw/litecell15-next' if d.getVar('NRW_BSP_DEVEL', False) == "next" else 'nrw/litecell15'}"
NO_SRCURI := ""
SRCURI := "git://${NRW_INTERNAL_MIRROR}/litecell15-diagtool.git;protocol=ssh;branch=${BRANCH}"

SRC_URI = "${@ '${SRCURI}' if d.getVar('NRW_INTERNAL_MIRROR', False) else '${NO_SRCURI}'}"

inherit autotools pkgconfig

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

do_install_append() {
       # create all symbolic link
       ln -sf /usr/bin/lc15diag ../image/usr/bin/getstatus
       ln -sf /usr/bin/lc15diag ../image/usr/bin/settxgain
       ln -sf /usr/bin/lc15diag ../image/usr/bin/settxbitstream
       ln -sf /usr/bin/lc15diag ../image/usr/bin/setrxgain
       ln -sf /usr/bin/lc15diag ../image/usr/bin/writetxreg
       ln -sf /usr/bin/lc15diag ../image/usr/bin/readtxreg
       ln -sf /usr/bin/lc15diag ../image/usr/bin/writerxreg
       ln -sf /usr/bin/lc15diag ../image/usr/bin/readrxreg
       ln -sf /usr/bin/lc15diag ../image/usr/bin/writefpgareg
       ln -sf /usr/bin/lc15diag ../image/usr/bin/readfpgareg
       ln -sf /usr/bin/lc15diag ../image/usr/bin/resetpwrcnt
       ln -sf /usr/bin/lc15diag ../image/usr/bin/readpwrinfo
       ln -sf /usr/bin/lc15diag ../image/usr/bin/setautopwr
       ln -sf /usr/bin/lc15diag ../image/usr/bin/resetvswrcnt
       ln -sf /usr/bin/lc15diag ../image/usr/bin/readvswrinfo
       ln -sf /usr/bin/lc15diag ../image/usr/bin/setpllpatch
       ln -sf /usr/bin/lc15diag ../image/usr/bin/getsysinfo
       ln -sf /usr/bin/lc15diag ../image/usr/bin/settf
       ln -sf /usr/bin/lc15diag ../image/usr/bin/muterf
       ln -sf /usr/bin/lc15diag ../image/usr/bin/setrxatten
       ln -sf /usr/bin/lc15diag ../image/usr/bin/isalive
       ln -sf /usr/bin/lc15diag ../image/usr/bin/setmaxcellsize
       ln -sf /usr/bin/lc15diag ../image/usr/bin/set8pskpowerred
       ln -sf /usr/bin/lc15diag ../image/usr/bin/setc0powerred
       ln -sf /usr/bin/lc15diag ../image/usr/bin/setl1tm
}


